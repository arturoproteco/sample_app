class User < ActiveRecord::Base
    EMAIL_REGEXP = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    
    validates :name,  presence: true,
                      length: { maximum: 100 }
    validates :email, presence: true,
                      length: { maximum: 255 },
                      format: { with: EMAIL_REGEXP },
                      uniqueness: { case_sensitive: false }
    validates :password, presence: true,
                         length: { minimum: 6 }
                      
    before_save { email.downcase! }
    
    has_secure_password
end
