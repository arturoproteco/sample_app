require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name: 'arturocadena', email: 'arturo@example.com',
                     password: 'hola1234', password_confirmation: 'hola1234')

  end
  
  test "el nombre debe estar presente" do
    @user.name = ''
    assert @user.invalid?
  end
  
  test "el email debe estar presente" do
    @user.email = ''
    assert @user.invalid?
  end
  
  test "el nombre no sea tan largo" do
    @user.name = 'a' * 101
    assert @user.invalid?
  end
  
  test "el email no debe ser tan largo" do
    @user.email = 'a' * 245 + '@example.com'
    assert @user.invalid?
  end
  
  test "debe aceptar los correos validos" do
    valid = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
                         
    valid.each do |valid_email|
      @user.email = valid_email
      assert @user.valid?
    end
  end
  
  test "debe rechazar los correos invalidos" do
    invalid = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
                           
    invalid.each do |invalid_email|
      @user.email = invalid_email
      assert @user.invalid?, "el email #{invalid_email} deberia ser incorrecto"
    end
  end
  
  test "email deberia ser unico" do
    @user.save
    new_user = @user.dup
    new_user.email = @user.email.upcase
    assert new_user.invalid?
  end
  
  test "el email debe guardarse en minusculas" do
    @user.email = "ArtUroPRoTEco@exAmplE.Com"
    @user.save
    assert_equal @user.email.downcase, @user.reload.email
  end
  
  test "el password debe estar presente" do
    @user.password = ' '
    assert @user.invalid?
  end
  
  test "el password no debe ser tan corto" do
    @user.password = @user.password_confirmation = 'a' * 5
    assert @user.invalid?
  end
end
